package amountmanager;

import accountmanager.MonetaryAmount;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class MonetaryAmountTest {

    MonetaryAmount monetaryAmount = new MonetaryAmount(500.99, "euros");


    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests MonetaryAmount");
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test MonetaryAmount");
    }

    @Test
    void testAddAmount(){
        monetaryAmount.addAmount(100);
        assertEquals(600.99, monetaryAmount.getAmount());
    }

    @Test
    void testSubstractAmountWithAmountParamLessCurrentAmount(){
        monetaryAmount.substractAmount(100);
        assertEquals(400.99, monetaryAmount.getAmount());
    }

    @Test
    void testSubstractAmountWithAmountParamGreaterCurrentAmount(){
        monetaryAmount.substractAmount(600.99);
        assertEquals(-100, monetaryAmount.getAmount());
    }

    @Test
    void testToString(){
        assertEquals(monetaryAmount.toString(), ("Le solde est de " + monetaryAmount.getAmount() +
                " " + monetaryAmount.getCurrency()));
    }

    @AfterEach
    void tearDown() {
        System.out.println("Fin du test MonetaryAmount");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("Fin des tests MonetaryAmount");
    }


}
