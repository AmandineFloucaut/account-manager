package amountmanager;

import accountmanager.SavingAccount;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class SavingAccountTest {

    SavingAccount savingAccount = new SavingAccount("Amandine", 10.00, 2,"euros");

    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests Account");
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test Account");
    }

    @Test
    void testCreditInterestMethod(){
        savingAccount.creditInterest();
        assertEquals("MonetaryAmount => amount = 10.02, currency = 'euros'", savingAccount.getCurrentBalance().toString());
    }

    /*@Test
    void testDepositMethod(){
        account.deposit(20);
        assertEquals("MonetaryAmount => amount = 20.0, currency = 'euros'", account.getCurrentBalance().toString());
    }*/

    /*@Test
    void testWithdrawMethodWithAmountNotAuthorized(){
        account.withdraw(1100);
        assertEquals("MonetaryAmount => amount = 0.0, currency = 'euros'", account.getCurrentBalance().toString());
    }

    @Test
    void testWithdrawMethodWithAmountParamLessBalance(){
        account.deposit(20);
        account.withdraw(10);
        assertEquals("MonetaryAmount => amount = 10.0, currency = 'euros'", account.getCurrentBalance().toString());
    }*/

    @AfterEach
    void tearDown() {
        System.out.println("Fin du test Account");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("Fin des tests Account");
    }

}
