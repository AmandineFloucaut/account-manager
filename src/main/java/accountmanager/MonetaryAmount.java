package accountmanager;

public class MonetaryAmount {


    private double amount;
    private final String currency; //WARNING => not make setter method

    public MonetaryAmount(double initialAmount, String currency){
        this.amount = initialAmount;
        this.currency = currency;
    }

    public void addAmount(double a){
        this.amount += a;
    }

    public void substractAmount(double a){
        this.amount -= a;
    }

    @Override
    public String toString() {
        return "Le solde est de " + amount + " " + currency;
    }

    //============ GETTERS & SETTERS ============//

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }
}
