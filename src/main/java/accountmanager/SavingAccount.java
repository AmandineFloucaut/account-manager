package accountmanager;

public class SavingAccount extends Account {

    private double interest; // percent

    public SavingAccount(String ownerName, double initialAmount, double interest, String currency){
        super(ownerName);

        this.interest = interest;
        if(initialAmount > 0){
            this.balance = new MonetaryAmount(initialAmount, currency);
        }
        else {
            System.out.println("Vous devez déposer au moins 0.50 cents pour ouvrir un compte épargne");
        }
    }

    public void creditInterest(){
        double newBalance = this.balance.getAmount() + (this.interest / 100);
        this.balance.setAmount(newBalance);
    }





}
