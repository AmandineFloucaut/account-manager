package accountmanager;

public class Account {

    protected MonetaryAmount balance;
    protected String ownerName;

    Account(String ownerName, String currency){
        this.ownerName = ownerName;
        this.balance = new MonetaryAmount(0, currency);
    }

    Account(String ownerName){
        this.ownerName = ownerName;
    }

    public void deposit(double amount){
        balance.addAmount(amount);
    }

    public void withdraw(double amount){
        // DOC - https://www.geeksforgeeks.org/double-compare-method-in-java-with-examples/;
        if(Double.compare(balance.getAmount(), amount) >= 0.1){
            balance.substractAmount(amount);
        }
    }

    public MonetaryAmount getCurrentBalance() {
        return balance;
    }

    public String getOwnerName() {
        return ownerName;
    }
}
