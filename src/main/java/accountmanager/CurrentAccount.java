package accountmanager;

public class CurrentAccount extends Account {

    public CurrentAccount(String ownerName, String currency){
        super(ownerName, currency);
    }
    
    public void deposit(double amount){
        balance.addAmount(amount);
    }

    @Override
    public void withdraw(double amount){

        if((balance.getAmount() + 1000.00) >= amount){
            balance.substractAmount(amount);
        }
    }
}
