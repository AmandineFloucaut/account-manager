import accountmanager.Account;
import accountmanager.CurrentAccount;
import accountmanager.SavingAccount;

public class Main {

    public static void main(String[] args){

        Account obelixCurrentAccount = new CurrentAccount("Obélix", "sesterces");
        SavingAccount obelixSavingAccount = new SavingAccount("Obélix", 1.00, 1, "sesterces");

        System.out.println("Compte courant - " + obelixCurrentAccount.getCurrentBalance());
        System.out.println("Compte épargne - " + obelixSavingAccount.getCurrentBalance());

        obelixCurrentAccount.deposit(100);
        System.out.println("Compte courant après dépôt 100 - " + obelixCurrentAccount.getCurrentBalance());

        obelixSavingAccount.deposit(30);
        System.out.println("Compte épargne après dépôt 30 - " + obelixSavingAccount.getCurrentBalance());

        obelixCurrentAccount.withdraw(200);
        System.out.println("Compte courant après retrait 200 - " + obelixCurrentAccount.getCurrentBalance());

        obelixSavingAccount.creditInterest();
        System.out.println("Compte épargne après ajout intérêts - " + obelixSavingAccount.getCurrentBalance());



    }
}